document.addEventListener("DOMContentLoaded", custom_add_to_cart);
function custom_add_to_cart() {
    if (!document.custom_processed) {
        var input = document.createElement("input");
        input.setAttribute("type", "hidden");
        input.setAttribute("name", "custom_add_to_cart_flag");
        input.setAttribute("value", 1);
        document.getElementById("product_addtocart_form").appendChild(input);
        document.custom_processed = true;//add field only once
    }
}