<?php

class Cymbio_Test_Model_Observer
{
    const SERVER_URL = 'https://server-staging.cym.bio/v1/events/';

    /**
     * send request to cymbio server on add to cart
     *
     * @param Varien_Event_Observer $observer
     * @return void
     */
    public function onAddToCart($observer)
    {
        $customFlag = Mage::app()->getRequest()->getParam('custom_add_to_cart_flag');
        if (!$customFlag) return;
        $store = Mage::app()->getStore();
        $postData = array(
            "action"     => "ADD_TO_CART",
            "referer"    => $store->getName(),
            "store_id"   => $store->getId(),
            "product_id" => $observer->getQuoteItem()->getProduct()->getId(),
        );
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, self::SERVER_URL);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, false);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $postData);
        curl_exec($curl);
        curl_close($curl);
    }
}
